const fs = require('fs');

const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')

function initialize(passport, getUserByEmail, getUserById) {
    const authenticateUser = async (email, password, done) => {
        const user = getUserByEmail(email);
        if (user == null) {
          return done(null, false, { message: 'No user with that email' });
        }
    
        try {
          const match = await bcrypt.compare(password, user.password);
          if (match) {
            return done(null, user);
          } else {
            return done(null, false, { message: 'Password incorrect' });
          }
        } catch (error) {
          return done(error);
        }
    };

    function getUserByEmail(email) {
        const usersData = fs.readFileSync('users.json', 'utf8');
        const users = JSON.parse(usersData);
        return users.find(user => user.email === email);
    }

    function getUserById(id) {
        const usersData = fs.readFileSync('users.json', 'utf8');
        const users = JSON.parse(usersData);
        return users.find(user => user.id === id);
      }
      

    passport.use(new LocalStrategy({usernameField: 'email'}, 
    authenticateUser))
    passport.serializeUser((user, done) => {
        done(null, user.id);
      });
      
      passport.deserializeUser((id, done) => {
        const user = getUserById(id);
        done(null, user);
      });
      
}

module.exports = initialize