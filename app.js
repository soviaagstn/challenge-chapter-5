// if(process.env.NODE_ENV !== 'production') {
//     require('dotenv').config()
// }

const express = require("express");
const app = express();
const fs = require("fs");
const passport = require("passport");
const bcrypt = require("bcrypt");
const flash = require("express-flash");
const session = require("express-session");
const methodOverride = require("method-override");
const { body, validationResult } = require("express-validator");

const port = 3000;

const initializePass = require("./passport-config");
initializePass(
  passport,
  (email) => users.find((user) => user.email === email),
  (id) => users.find((user) => user.id === id)
);

// untuk layout dr ejs
const expressLayouts = require("express-ejs-layouts");

const users = [];
const sessionSecret = "your-secret-value";
// gunakan ejs
app.set("view engine", "ejs");
// layout dr ejs
app.use(expressLayouts);
// built in middleware / third party middleware
app.use(express.static("public"));

app.use(express.urlencoded({ extended: false }));
app.use(flash());
app.use(
  session({
    secret: sessionSecret,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride("_method"));

app.get("/", checkUserAuth, (req, res) => {
  res.render("landing-page", {
    layout: "layouts/main-layout",
    title: "Halaman About",
    name: req.user.name,
  });
});
app.get("/work-page", checkUserAuth, (req, res) => {
  res.render("work-page", {
    layout: "layouts/main-layout",
    title: "Halaman work",
    name: req.user.name,
  });
});
app.get("/about-me-page", checkUserAuth, (req, res) => {
  res.render("about-me", {
    layout: "layouts/main-layout",
    title: "Halaman About",
    name: req.user.name,
  });
});
app.get("/contact-page", checkUserAuth, (req, res) => {
  res.render("contact", {
    layout: "layouts/main-layout",
    title: "Halaman contact",
    name: req.user.name,
  });
});
app.get("/login", checkUserNotAuth, (req, res) => {
  res.render("login", {
    layout: "layouts/login-layout",
    title: "Halaman login",
  });
});
app.get("/register", checkUserNotAuth, (req, res) => {
  res.render("register", {
    layout: "layouts/register-layout",
    title: "Halaman register",
  });
});
app.delete("/logout", (req, res) => {
  req.logOut((err) => {
    //requires a callback function (otherwise error)
    if (err) {
      console.log(err);
    }
    res.redirect("/login");
  });
});

// pakai passport auth middleware for login
app.post(
  '/login',
  checkUserNotAuth,
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
  })
);

function isValidEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  app.post("/register", checkUserNotAuth, async (req, res) => {
    const { name, email, password } = req.body;
    let errors = [];
    // -------validation rules
    if (!name) {
      errors.push({ msg: "Name is required" });
    } else if (name.length < 8) {
      errors.push({ msg: "Name must be at least 8 characters" });
    }
  
    if (!email) {
      errors.push({ msg: "Email is required" });
    } else if (!isValidEmail(email)) {
      errors.push({ msg: "Invalid email format" });
    }
  
    if (!password) {
      errors.push({ msg: "Password is required" });
    } else if (password.length < 8) {
      errors.push({ msg: "Password must be at least 8 characters" });
    }
  
    //   end of validation rules---------
  
    if (errors.length > 0) {
      res.render("register", {
        layout: "layouts/register-layout",
        title: "Halaman register",
        errors: errors,
      });
    } else {
      try {
        const hashedPass = await bcrypt.hash(password, 10);
        const user = {
          id: Date.now().toString(),
          name,
          email,
          password: hashedPass,
        };
  
        // Read existing user data from the file
        let userData = [];
        try {
          const data = fs.readFileSync("users.json");
          userData = JSON.parse(data);
        } catch (err) {
          console.error("Error reading users.json:", err);
        }
  
        // Add the new user to the array
        userData.push(user);
  
        // Save the updated user data to the file
        try {
          fs.writeFileSync("users.json", JSON.stringify(userData, null, 2));
        } catch (err) {
          console.error("Error writing users.json:", err);
        }
  
        res.redirect("/login");
      } catch (err) {
        console.error("Error saving user data:", err);
        res.redirect("/register");
      }
    }
  });

app.use("/", (req, res) => {
  res.status(404);
  res.send("Hi, unfortunately this page doesn;t exist :<");
});

// middleware function untuk cek authenticate currently logged in user
function checkUserAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/login");
}

// middleware untuk menghindari login saat sudah login (logout)
function checkUserNotAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }

  next(); //if they're not logged in (authenticated), continue
}

app.listen(port, () => {
  console.log(`Server is listening on port ${port}..`);
});
