const selectionButtons = document.querySelectorAll(".selection");
const player1Selection = document.querySelector(
  ".player__data:first-child .selection" //index pertama dr selection player1 (not working)
);
const computerSelection = document.querySelector(
  ".com__data:last-child .selection" // pilihan terakhir dari komputer (not working)
);
const finalResultText = document.getElementById("final-result-text");
const refreshButton = document.getElementById("refresh-button");

class Player {
  constructor(name) {
    this.name = name;
    this.beats = null;
    this.winner = null;
  }
}

class Game {
  constructor() {
    this.player1 = new Player("Player 1");
    this.computer = new Player("Computer");

    selectionButtons.forEach((button) => {
      button.addEventListener("click", () => {
        this.setPlayerChoice(button.getAttribute("data-selection"));
        this.generateComputerChoice();
        this.determineWinner();
      });
    });

    refreshButton.addEventListener("click", () => {
      window.location.reload(); //reload
    });
  }

  setPlayerChoice(beats) {
    this.player1.beats = beats;
    this.player1.result = beats;

    // alternatif backgroundColor dengan cara di print hasil selectionnya
    const outputElement = document.getElementById("output-result");
    outputElement.textContent = this.player1.result;

    // ka, maaf backgroundColornya belum beekerja, jadi aku alternatifin menggunakan cara diatas... tapi gamenya bekerja kok ka..
    // terima kasih ka..
    // player1Selection.style.backgroundColor = "yellow"
    console.log(this.player1.result);
  }

  generateComputerChoice() {
    const beats = ["rock", "paper", "scissors"];
    const randomIndex = Math.floor(Math.random() * beats.length);
    const computerChoice = beats[randomIndex];

    this.computer.beats = computerChoice;
    this.computer.result = computerChoice;

    // alternatif backgroundColor dengan cara di print hasil selectionnya
    const outputElements = document.getElementById("output-results");
    outputElements.textContent = this.computer.result;

    // ka, maaf backgroundColornya belum beekerja, jadi aku alternatifin menggunakan cara diatas... tapi gamenya bekerja kok ka..
    // terima kasih ka..
    // computerSelection.style.backgroundColor = "yellow"
    console.log(this.computer.result);
  }

  determineWinner() {
    const SELECTIONS = [
      { name: "rock", beats: "rock" },
      { name: "rock", beats: "paper" },
      { name: "rock", beats: "scissors" },
      { name: "paper", beats: "rock" },
      { name: "paper", beats: "paper" },
      { name: "paper", beats: "scissors" },
      { name: "scissors", beats: "rock" },
      { name: "scissors", beats: "paper" },
      { name: "scissors", beats: "scissors" },
    ];

    const { beats: player1Choice } = this.player1;
    const { beats: computerChoice } = this.computer;

    if (player1Choice === computerChoice) {
      finalResultText.textContent = "DRAW";
      finalResultText.style.backgroundColor = "#035B0C";
      finalResultText.style.color = "white";
      finalResultText.style.fontWeight = "bold";
      finalResultText.style.fontSize = "20px";
      finalResultText.style.width = "fit-content";
      finalResultText.style.padding = "10%";
      finalResultText.style.borderRadius = "10px";
      finalResultText.style.transform = "rotate(-30deg)";
    } else {
      const winningSelection = SELECTIONS.find(
        (selection) =>
          selection.name === player1Choice && selection.beats === computerChoice
      );

      if (winningSelection) {
        finalResultText.textContent = "PLAYER 1 WIN";
        finalResultText.style.backgroundColor = "#4C9654";
        finalResultText.style.color = "white";
        finalResultText.style.fontWeight = "bold";
        finalResultText.style.fontSize = "20px";
        finalResultText.style.width = "fit-content";
        finalResultText.style.padding = "10%";
        finalResultText.style.borderRadius = "10px";
        finalResultText.style.transform = "rotate(-30deg)";
      } else if (player1Choice !== null && computerChoice !== null) {
        finalResultText.textContent = "COM WIN";
        finalResultText.style.backgroundColor = "#4C9654";
        finalResultText.style.color = "white";
        finalResultText.style.fontWeight = "bold";
        finalResultText.style.fontSize = "20px";
        finalResultText.style.width = "fit-content";
        finalResultText.style.padding = "10%";
        finalResultText.style.borderRadius = "10px";
        finalResultText.style.transform = "rotate(-30deg)";
      }
    }
  }
}

const game = new Game();
